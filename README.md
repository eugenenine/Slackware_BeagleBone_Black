# Slackware on BeagleBone Black

![](Screenshot_2023-12-11_18-44-31.png)

## History

I bought a [BeagleBone](https://www.beagleboard.org/) [Black](https://www.beagleboard.org/boards/beaglebone-black) in January of 2015 as an upgrade to the original Raspberry Pi I was using to host my home Owncloud server. I followed some forum posts to use another distro's Kernel with an install of Slackware and had it basically running. I've since replaced it with a Raspebrry Pi 2 and later a 3 etc and replaced Owncloud with Nextcloud and left the Beagle sitting until this year (2023) when I was cleaning up and decided I'd either get it running again or get rid of it.

## Pre-req
* Slackware 15 minirootfs from http://ftp.arm.slackware.com/slackwarearm/slackwarearm-devtools/minirootfs/roots/ (BeagleBone Black is not 64 bit so you do not want the arch64)
* Kernel and boot files from this repo
* If you want to compile your own kernel you will also need:
	* firmware from https://git.ti.com/cgit/processor-firmware/ti-amx3-cm3-pm-firmware/tree/bin?id=7eb9c0856a9e8b3b42bf64f761da135852b8eea7
	* wireless regdb https://mirrors.edge.kernel.org/pub/software/network/wireless-regdb/ if you want to support wireless adapters

## SD Card Creation

Replace **sdX** in the following instructions with the device name for the SD card as it appears on your computer.

1. Zero the beginning of the SD card:

        dd if=/dev/zero of=/dev/sdX bs=1M count=8

2. Start fdisk to partition the SD card:

        fdisk /dev/sdX

3.    At the fdisk prompt, delete old partitions and create a new one:
        Type **o**. This will clear out any partitions on the drive.
        Type **p** to list partitions. There should be no partitions left.
        Now type **n**, then **p** for primary, **1** for the first partition on the drive, **2048** for the first sector, and then press ENTER to accept the default last sector.
        Write the partition table and exit by typing **w**.
4. Create the ext4 filesystem:

        mkfs.ext4 /dev/sdX1

5. Mount the filesystem:

        mkdir mnt
        mount /dev/sdX1 mnt

6. Download and extract the Slackware miniroot filesystem:

        wget http://ftp.arm.slackware.com/slackwarearm/slackwarearm-devtools/minirootfs/roots/slackarm-15.0-miniroot_15Feb22.tar.xz
        tar -xpf slackarm-15.0-miniroot_15Feb22.tar.xz -C mnt
        sync
7. Download the boot files from here and place in boot
8. Install the U-Boot bootloader:

        dd if=mnt/boot/MLO of=/dev/mmcblk1 count=1 seek=1 conv=notrunc bs=128k
        dd if=mnt/boot/u-boot.img of=/dev/mmcblk1 count=2 seek=1 conv=notrunc bs=384k
        umount mnt
        sync

9. Insert the SD card into the BeagleBone, connect ethernet, and apply 5V power while holding down the switch on the top of the board near the micro SD slot.
 
10.    Use the serial console or SSH to the IP address given to the board by your router.

## Completing the setup
		
* Time - since the BBB has no RTC you will want to setup ntp and set it to start at boot.
		
		nano /erc/ntp.conf
		chmod +x rc.ntpd

* Update/Install full Slackware - set a mirror for Slackpkg, remember to use the non-64 bit arm.

		nano /erc/slackpkg/mirrors
		slackpkg update

	update

		slackpkg upgrade-all	

		or if you have space and wish install the full slackware

		slackpkg install slackware

* make your own kernel - if you want to compile your own kernel, start as normal with the source and the current config either from the running kernel or from this repo

		cd /usr/src
		cd linux-5.15.116/
		zcat /proc/config.gz > .config
		make olddefconfig		

	Now you need the firmware.

		mkdir firmware
		wget https://git.ti.com/cgit/processor-firmware/ti-amx3-cm3-pm-firmware/tree/bin/am335x-bone-scale-data.bin
		wget https://git.ti.com/cgit/processor-firmware/ti-amx3-cm3-pm-firmware/tree/bin/am335x-evm-scale-data.bin
		wget https://git.ti.com/cgit/processor-firmware/ti-amx3-cm3-pm-firmware/tree/bin/am335x-pm-firmware.bin
		wget https://git.ti.com/cgit/processor-firmware/ti-amx3-cm3-pm-firmware/tree/bin/am335x-pm-firmware.elf
		wget https://git.ti.com/cgit/processor-firmware/ti-amx3-cm3-pm-firmware/tree/bin/am43x-evm-scale-data.bin
		wget https://mirrors.edge.kernel.org/pub/software/network/wireless-regdb/wireless-regdb-2023.09.01.tar.xz
		tar xvf wireless-regdb-2023.09.01.tar.xz

	then continue with the kernel build as normal

		make
		make bzImage
		make modules
		make modules_install

	Once complete rename your old kernel in boot and copy the new in its place.