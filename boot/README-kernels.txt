#######################################################################
# Document: /boot/README-kernels.txt
# Purpose:  Explain the various files
# Author:   Stuart Winter <mozes@slackware.com>
# Date:     23-May-2021
# Version:  1.03
#######################################################################
# Changelog:
# v1.03, 23-May-2021
# Updated for AArch64.
# v1.02, 08-Aug-2019
#######################################################################

With a full installation of Slackware 15.0, Slackware ARM / AArch64
contains a single Kernel that supports all supported Hardware Models.

In the illustrations below, we are covering Slackware ARM/32bit ARMv7.
Slackware AArch64 is identical apart from the architecture name is
'armv8'.

   kernel_armv7-<Linux version>-arm-<build number>

These packages contain kernels for a variety of ARMv7 Hardware Models.

In the /boot directory, you will see a number of symbolic links and their
corresponding versioned file.  Here is a short run down:-

   zImage-armv7

     The standard gzip compressed version of the Linux Kernel.

     Most of the systems supported by Slackware ARM use this copy of
     the Kernel.

   initrd-armv7

     The gzip compressed Initial RAM disk that contain the drivers
     (Kernel modules) required to boot the system (Hardware Model).
     
   dtb/*

     These are the Device Tree Blob files that are used to describe
     the ARM Hardware Model that you are booting.  

     All supported Hardware Models make use of the DTB.

If you have any questions or need help, please register and post
your question at the officially supported Slackware ARM forum:
  http://www.linuxquestions.org/questions/slackware-arm-108/

-- 
Stuart Winter
<mozes@slackware.com>
